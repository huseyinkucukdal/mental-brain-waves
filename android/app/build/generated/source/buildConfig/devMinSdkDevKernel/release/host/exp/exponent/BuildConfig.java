/**
 * Automatically generated file. DO NOT MODIFY
 */
package host.exp.exponent;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "app.pochasoft.mentalbrainwavesfree";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "devMinSdkDevKernel";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
  public static final String FLAVOR_minSdk = "devMinSdk";
  public static final String FLAVOR_remoteKernel = "devKernel";
}
