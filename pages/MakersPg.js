import React from 'react';
import { View, Text,  StyleSheet, Dimensions, Image, ScrollView, TouchableHighlight, ImageBackground, StatusBar, SafeAreaView } from 'react-native';
import { ListItem } from "react-native-elements";
import { Col, Row, Grid } from "react-native-easy-grid";
import {Icon} from "react-native-elements";

const BACKGROUND_COLOR = '#fff';

const list = [
  {
    name: 'David Brown',
    avatar_url: 'https://randomuser.me/api/portraits/men/85.jpg',
    subtitle: 'Project Manager'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://randomuser.me/api/portraits/women/2.jpg',
    subtitle: 'Sound Consultant'
  },
  {
    name: 'Raymond Mendoza',
    avatar_url: 'https://randomuser.me/api/portraits/men/5.jpg',
    subtitle: 'Sound Designer'
  },
  {
    name: 'Miguel Dean',
    avatar_url: 'https://randomuser.me/api/portraits/men/29.jpg',
    subtitle: 'Developer'
  },
  {
    name: 'Tracy Newman',
    avatar_url: 'https://randomuser.me/api/portraits/women/32.jpg',
    subtitle: 'Designer'
  },
  {
    name: 'Elijah Peterson',
    avatar_url: 'https://randomuser.me/api/portraits/men/3.jpg',
    subtitle: 'Designer'
  },
  {
    name: 'Seth Jordan',
    avatar_url: 'https://randomuser.me/api/portraits/men/54.jpg',
    subtitle: 'Developer'
  },
  {
    name: 'Nathan Phillips',
    avatar_url: 'https://randomuser.me/api/portraits/men/11.jpg',
    subtitle: 'Developer'
  },
  {
    name: 'Ian Obrien',
    avatar_url: 'https://randomuser.me/api/portraits/men/12.jpg',
    subtitle: 'UX Designer'
  },
  {
    name: 'Ufuk Kargaci',
    avatar_url: 'https://randomuser.me/api/portraits/men/22.jpg',
    subtitle: 'Test Specialist'
  },
]


export default class MakersPg extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cardNumber : 0
    };
  }

  static navigationOptions = {
    title: 'Creators',
    headerStyle: {
      backgroundColor: '#000000',
    },
    headerTintColor: 'white',
  };

  onPress = (categoryName) => {
    this.props.navigation.navigate('WelcomePage');
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#000000"
          barStyle="light-content"
        />
        <Grid>
          <Row size={100}>
            <Col>
              <View style={{height:100}}>
                <ImageBackground source={require('../assets/images/logo.png')} style={{width: '100%', height: '100%'}}>
                  <View style={{width:"100%", height:70}}>

                  </View>
                </ImageBackground>
              </View>
              <ScrollView>
                {
                  list.map((l, i) => (
                    <ListItem
                      key={i}
                      rightIcon={<Icon name="" color="#a2a2a2"/>}
                      avatar={{uri:l.avatar_url}}
                      title={l.name}
                      subtitle={l.subtitle}
                    />
                  ))
                }
              </ScrollView>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    flexDirection: 'column'
  }
});











