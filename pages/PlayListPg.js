import React from 'react';
import { View, StyleSheet, Dimensions, Image, ScrollView, TouchableHighlight, StatusBar, AsyncStorage, Button, NetInfo, Text,TouchableWithoutFeedback, ActivityIndicator, Animated, TouchableOpacity } from 'react-native';
import * as _  from "lodash";
import songLsAll from "../assets/SongLst/songLsAll";
import { Asset } from "expo";
import { DText } from "../components";
import { Constants, WebBrowser } from 'expo';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo';
import {connect} from "react-redux";
import * as Animatable from 'react-native-animatable';

const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get('window');
const BACKGROUND_COLOR = '#fff';
const DISABLED_OPACITY = 0.5;
const FONT_SIZE = 14;
const LOADING_STRING = '... loading ...';
const BUFFERING_STRING = '...buffering...';
const RATE_SCALE = 3.0;
const VIDEO_CONTAINER_HEIGHT = DEVICE_HEIGHT * 2.0 / 5.0 - FONT_SIZE * 2;


class Icon {
  constructor(module, width, height) {
    this.module = module;
    this.width = width;
    this.height = height;
    Asset.fromModule(this.module).downloadAsync();
  }
}

const ICON_TRACK_1 = new Icon(require('../assets/images/track_1.png'), 320, 5);
const ICON_PLAY_1 = new Icon(require('../assets/images/play_button.png'), 20, 20);
const ICON_PLAY_2 = new Icon(require('../assets/images/lockIcon.png'), 20, 20);
const ICON_PLAY_3 = new Icon(require('../assets/images/notPlayIcon.png'), 20, 20);



AdMobRewarded.setAdUnitID('ca-app-pub-7428875399570236/2292945138'); // Test ID, Replace with your-admob-unit-id
AdMobRewarded.setTestDeviceID('EMULATOR');

Animatable.initializeRegistryWithDefinitions({
  myFancyAnimation: {
    0: {
      top: 0,
      scale: 1,
      fontSize: 12
      
    },
    0.5: {
      top: -13,
      fontSize: 18,
      scale: 3
    },
    1: {
      top: 0,
      fontSize: 12,
      scale: 1
    },
  }
});

const Comp = class extends React.Component {


  constructor(props){
    super(props);
    this.selectedCategory = this.props.navigation.getParam('selectedCategory');
    this.state = {
      result: null,
      isConnected: "connected",
      isAnimating: false
    }
    this.isConnected = this.isConnected.bind(this);
    this.bounce = this.bounce.bind(this);
  }

  static navigationOptions = {
    title: 'List',
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: 'white',
  };


  componentDidMount(){
    this.isConnected();
  }

  async openReklam(){
    await AdMobRewarded.requestAdAsync();
    await AdMobRewarded.showAdAsync();

    await AdMobRewarded.addEventListener('rewardedVideoDidClose',
      async () => {
        this.setState({isAnimating: false});
        await this._browserButtonAsync();
      }
    );
  }

  _storeData(song, idx){
    if(this.state.isConnected === "connected"){
      const changedSongList = this.props.songListLockState.map((s)=>{
        let selectedSong = s;
        if(song.name === s.name){
          selectedSong.currentNumber = selectedSong.currentNumber + 1;
          if(selectedSong.currentNumber >= selectedSong.totalWatchNumber){
            selectedSong.isLocked = false;
          }
        }
        return selectedSong;
      });
      this.props.setSongListLockState(changedSongList);
    }
  }

  isConnected = ()=>{
      NetInfo.isConnected.fetch().then(isConnected => {
        this.setState({isConnected: isConnected ? "connected":"notConnected"});
      });
    if(this.state.isConnected !== "connected"){
      setTimeout(()=>{
        this.isConnected();
      }, 3000);
    }
  }

  navigateToSong = (id) => {
    this.props.navigation.navigate('PlayPage', {
      itemId: id,
      songName: songLsAll[id].name,
    });
    this.forceUpdate();
  }

  _browserButtonAsync = async () => {
    let result = await WebBrowser.openBrowserAsync('https://play.google.com/store/apps/details?id=com.tencent.ig');
    this.setState({ result });
  };


  bounce = (elem, songName) => {
    this.setState({isAnimating: true});
    this[elem].pulse(1000).then(endState => {
      if(endState.finished){
        // this.setState({isAnimating: false});
      }
    })
    this[songName].myFancyAnimation(1500).then(endState => console.log(endState.finished ? 'bounce finished' : 'bounce cancelled'))


  };

  async onPressSong(song, id){
    if(this.state.isConnected === "connected" && !this.state.isAnimating){
      if(!song.isLocked){
        console.log("sarki kilitli degil");
        this.navigateToSong(id);
      }else{
        console.log("reklam aciliyor");
        this.bounce(id, song.name);
        this.openReklam();
        console.log("reklam bitti");
        this.isConnected();
        this._storeData(song, id);
      }
    }
  }

  handleViewRef = ref => this.view = ref;

  render() {

    const animatedStyle = {
      backgroundColor: "white",
      paddingTop: 0,
      paddingBottom:0,
    }

    const animatedSize = {
      width: 20,
      height: 20,
      borderRadius: 30,
      marginTop: 12,
      marginRight: 5,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "black"
    }

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="black"
          barStyle="light-content"
        />
        {this.state.isConnected === "connected" &&
          <View style={{justifyContent:"center", alignItems: "center"}}>
            <PublisherBanner
              bannerSize="banner"
              adUnitID="ca-app-pub-7428875399570236/6884046395" // Test ID, Replace with your-admob-unit-id
              testDeviceID="EMULATOR"
            />
          </View>
        }
        {this.state.isConnected !== "connected" &&
          <View style={{padding: 15, backgroundColor: "red", flexDirection: 'row',justifyContent: 'center', alignItems: "center"}}>
            <ActivityIndicator size="small" color="#fff" />
            <View style={{paddingLeft: 10}}>
              <DText style={{color:"#fff", fontSize:16}}>Checking Internet Connection...</DText>
            </View>
          </View>
        }
        <ScrollView>
        {_.map(this.props.songListLockState, (song, id)=>{
          if(song.category === this.selectedCategory){
            return (
              <TouchableHighlight onPress={()=>this.onPressSong(song, id)} key={id} underlayColor="transparent">
                <Animatable.View ref={ref => this[id] = ref}>
                <View  style={[animatedStyle]}>
                  <View style={styles.blockContainer}>
                    <View style={styles.barContainer}>
                      <DText style={styles.blockText}>{_.get(song,"name","*unknown")}</DText>
                      <DText style={styles.blockText}>{_.get(song,"duration","*unknown")}</DText>
                    </View>
                    <View style={styles.numberArea}>
                      {song.isLocked && (
                        <Animatable.View ref={ref => this[song.name] = ref}>
                          <View style={styles.counterWrapper}>
                            <Text style={{color: "#ffff"}}>{`${_.get(song,"totalWatchNumber","*unknown") - _.get(song,"currentNumber","*unknown")}`}</Text>
                          </View>
                        </Animatable.View>
                      )}
                      <Image
                        source={song.isLocked ? ICON_PLAY_3.module : ICON_PLAY_1.module}
                        style={styles.playButton}
                      />
                    </View>
                  </View>
                  <View style={{width: "100%", height:5}}>
                    <Image
                      source={ICON_TRACK_1.module}
                      style={styles.bar}
                    />
                  </View>
                </View>
                </Animatable.View>
              </TouchableHighlight>
            )
          }
        })}
        </ScrollView>
        {this.state.isConnected === "connected" &&
          <View style={{justifyContent:"center", alignItems: "center"}}>
            <AdMobBanner
              bannerSize="banner"
              adUnitID="ca-app-pub-7428875399570236/8034719808" // Test ID, Replace with your-admob-unit-id
              testDeviceID="EMULATOR"
              onDidFailToReceiveAdWithError={this.bannerError} />
          </View>
        }
      </View>
    );
  }
}


function mapStateToProps(state){
  return {
    songListLockState: state.songListLockState
  }
}

function mapDispatchToProps(dispatch){
  return {
    setSongListLockState: (newSongList)=> dispatch({type: "setSongListLockState", newSongList: newSongList})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Comp);

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    flexDirection: 'column',
    paddingBottom: 0
  },
  block:{
    paddingTop: 0,
    paddingBottom:0,
  },
  barContainer: {
    paddingBottom: 10,
    paddingTop:10
  },
  bar:{
    minWidth: "100%",
    width: DEVICE_WIDTH,
    resizeMode: "stretch",
  },
  numberArea: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: 'row',
    marginTop: -8
  },
  counterWrapper: {
    width: 20,
    height: 20,
    borderRadius: 30,
    marginTop: 12,
    marginRight: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black"
  },
  blockContainer:{
    flexDirection: 'row',
    // flex:1,
    justifyContent: 'space-between',
    alignItems: "center"
  },
  blockText: {
    paddingLeft: 10,
    paddingRight: 5,
    fontSize: 18
  },
  time:{
    paddingLeft: 10,
    paddingRight: 5
  },
  playButton: {
    height: 30,
    width: 30,
    resizeMode: "stretch",
    marginTop: 15,
    marginRight: 10
  }
});



