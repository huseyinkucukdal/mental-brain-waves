import React from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, StatusBar, AsyncStorage } from 'react-native';
import { DText } from "../components";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Button } from "react-native-elements";
import {connect} from "react-redux";
// import songLsAll from '../assets/SongLst/songLsAll';

const BACKGROUND_COLOR = '#000000';

const Comp = class WelcomePage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isLogoShowed : false
    };
  }

  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      backgroundColor: '#fbecc8',
    },
    headerTintColor: 'black',
    header: null
  };

   componentDidMount(){
    this.passSongsFromStoreToState();
  }


  passSongsFromStoreToState = ()=>{
     this.setDefaultValues();
     this.setIsLogoShowed();
  }

  setDefaultValues = async()=>{

    try {
      const value = await AsyncStorage.getItem("storageSongList");

      if (value !== null) {
        // await AsyncStorage.setItem("storageSongList",JSON.stringify(this.props.songListLockState));
        this.props.setSongListLockState(value);


      }
      else{
        // when app is installed at first
        try{
          await AsyncStorage.setItem("storageSongList",JSON.stringify(this.props.songListLockState));
        }catch (er){
          console.log("bd error");
        }

      }
    }
    catch (error) {
      // Error retrieving data
      console.log("Error retrieving data");

    }
  }

  setIsLogoShowed = ()=>{
    setTimeout(()=>{
      this.setState({isLogoShowed: true})
    }, 3000);
  }

  onPress = (pageName) => {
    this.props.navigation.navigate(pageName);
  }

  render() {
    if(!this.state.isLogoShowed){
      return (
        <View>
          <StatusBar hidden/>
          <Image
            style={{resizeMode:"cover",
              alignSelf: 'center',
              height: "100%",
              width: "100%"
            }}
            source={require('../assets/images/intro.png')}
          />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#fff"
          color="black"
        />
        <Grid>
          <Row size={50}>
            <Col>
              <ImageBackground source={require('../assets/images/welcomeLogo.png')} style={{width: '100%', height: '100%'}}>
                <View style={{width:"100%", height:"100%"}}>

                </View>
              </ImageBackground>
            </Col>
          </Row>
          <Row size={15}>
            <Col>
              <View style={{width: 210, justifyContent:"center", alignSelf:"center"}}>
                <Button
                  onPress={()=>this.onPress("CategoryPage")}
                  buttonStyle={styles.footerButton}
                  rounded
                  backgroundColor="#fff"
                  color="#000000"
                  title={<DText style={{letterSpacing:2, fontSize:20}}>LISTEN</DText>}
                />
              </View>
            </Col>
          </Row>
          <Row size={25}>
            <Col>
              <View style={{width: 210, justifyContent:"center", alignSelf:"center"}}>
                <Button
                  onPress={()=>this.onPress("GuidePage")}
                  buttonStyle={styles.footerButton}
                  rounded
                  backgroundColor="#fff"
                  color="#000000"
                  title={<DText style={{letterSpacing:2, fontSize:20}}>USER GUIDE</DText>}
                />
              </View>
            </Col>
          </Row>
          <Row size={10} style={styles.footer}>
            <Col>
              <View style={{width: 150, justifyContent:"center", alignSelf:"center"}}>
                <Button
                  onPress={()=>this.onPress("MakersPage")}
                  buttonStyle={styles.footerButton}
                  rounded
                  backgroundColor="black"
                  color="white"
                  title={<Text style={{letterSpacing:2, fontSize:16}}>creators</Text>}
                />
              </View>
            </Col>
          </Row>
        </Grid>
      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    songListLockState: state.songListLockState
  }
}

function mapDispatchToProps(dispatch){
  return {
    setSongListLockState: (newSongList)=> dispatch({type: "setSongListLockState", newSongList: newSongList})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Comp);

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    flexDirection: 'column',
  },
  footer:{
    backgroundColor: "white",
    alignItems: "center"
  },
  footerButton:{

  }

});











