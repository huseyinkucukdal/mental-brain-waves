import React from 'react';
import { View, Text,  StyleSheet, StatusBar, NetInfo } from 'react-native';
import { DText } from "../components";
import { Card, Button } from "react-native-elements";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo';

const BACKGROUND_COLOR = '#fff';

const cardList = [
  {
    imgUrl: require('../assets/images/earphoneIcon.png'),
    title: 'WEAR YOUR EARPHONE',
    text: "Efficiency of wearing earphone is 70% higher. Because outside sound frequency can deregulate big amount of binaural beats frequency. It can cause resonance or damping. In addition, earphone sounds better than speakers by this way focusing performance will increase. An good quality earphone should be used."
  },
  {
    imgUrl: require('../assets/images/lightIcon.png'),
    title: 'TURN OFF LIGHT',
    text: "To see stuff around can lose your concentration. Closing light is the best solution of gaining concentration. Even curtain should be closed. Without light sound will get better effect.",
  },
  {
    imgUrl: require('../assets/images/eyesIcon.png'),
    title: 'CLOSE YOUR EYES',
    text: "To close eyes will increase your concentration. Then you will get better effect. Also you will feel more relax and ready for your dose. In addition your brain will focus to process binaural beats frequency.",
  },
  {
    imgUrl: require('../assets/images/bedIcon.png'),
    title: 'LIE DOWN',
    text: "Lying down is one of steps for relaxing body and concentration. More over, putting off clothes except underwear will make it easy for that body relaxation for interaction to binaural beats frequency.",
  },
  {
    imgUrl: require('../assets/images/slienceIcon.png'),
    title: 'SILENT AMBIENT',
    text: "Provide silent ambient because of that it is another important point to focus sound frequency. You should turn off every device cause loud ambition.",
  },
];


export default class GuidePg extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cardNumber : 0,
      isConnected: "connected"
    };
    this.onChangeCard = this.onChangeCard.bind(this);
  }

  static navigationOptions = {
    title: 'Guide',
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: 'white'
  };

  async onExitGuide(){
    if(this.state.isConnected === "connected"){
      AdMobInterstitial.setAdUnitID('ca-app-pub-7428875399570236/5662749470'); // Test ID, Replace with your-admob-unit-id
      AdMobInterstitial.setTestDeviceID('EMULATOR');
      await AdMobInterstitial.requestAdAsync();
      await AdMobInterstitial.showAdAsync();
    }
    setTimeout(()=>{
      this.props.navigation.navigate('WelcomePage');
    },1000);
  }

  componentDidMount(){
    this.isConnected();
  }

  isConnected = ()=>{
    NetInfo.isConnected.fetch().then(isConnected => {
      this.setState({isConnected: isConnected ? "connected":"notConnected"});
    });
    if(this.state.isConnected !== "connected"){
      setTimeout(()=>{
        this.isConnected();
      }, 3000);
    }
  }

  onPress = (categoryName) => {

  }

  onChangeCard = () =>{
    if(cardList.length === this.state.cardNumber + 1){
      this.onExitGuide();
    }
    else{
      this.setState({cardNumber: this.state.cardNumber+1});
    }

  }


  render() {

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#3716b0"
          barStyle="light-content"
        />
        <View style={{width: "100%", alignItems: "center", justifyContent: "center", marginTop: 20, marginBottom: 5}}>
          <DText style={{fontSize: 20}}>LEARN FOR BETTER EFFECT</DText>
        </View>
        <Card
          title={cardList[this.state.cardNumber]["title"]}
          image={cardList[this.state.cardNumber]["imgUrl"]}>
          <Text style={{marginBottom: 10}}>
            {cardList[this.state.cardNumber]["text"]}
          </Text>
          <Button
            onPress={()=>this.onChangeCard()}
            backgroundColor='black'
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title={cardList.length === this.state.cardNumber + 1 ? "EXIT" : "NEXT"} />
        </Card>
        <View style={{alignSelf:"flex-end"}}>
          {this.state.isConnected === "connected" &&
            <View style={{justifyContent: "center", alignItems: "center"}}>
              <AdMobBanner
                bannerSize="largeBanner"
                adUnitID="ca-app-pub-7428875399570236/8034719808" // Test ID, Replace with your-admob-unit-id
                testDeviceID="EMULATOR"
                onDidFailToReceiveAdWithError={this.bannerError} />
            </View>
          }
        </View>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    height: "100%",
    flexDirection: 'column'
  }
});











