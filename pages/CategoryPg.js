import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight, StatusBar, NetInfo } from 'react-native';
import { DText } from "../components";
import healthIcon from "../assets/images/healthIcon.png";
import meditationIcon from "../assets/images/meditationIcon.png";
import drugsIcon from "../assets/images/drugsIcon.png";
import feelingIcon from "../assets/images/feelingIcon.png";
import sexualityIcon from "../assets/images/sexualityIcon.png";
import improvementIcon from "../assets/images/improvementIcon.png";
import { Col, Row, Grid } from "react-native-easy-grid";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo';

const BACKGROUND_COLOR = '#fff';



export default class CategoryPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isConnected: "connected"
    }
    this.isConnected = this.isConnected.bind(this);
  }
  componentDidMount(){
    this.isConnected();
  }

  static navigationOptions = {
    title: 'Categories',
    headerStyle: {
      backgroundColor: '#000',
    },
    headerTintColor: '#fff',
  };
  onPress = (categoryName) => {
    this.props.navigation.navigate('PlayListPage', {
      selectedCategory: categoryName
    });
  }

  isConnected = ()=>{
    NetInfo.isConnected.fetch().then(isConnected => {
      this.setState({isConnected: isConnected ? "connected":"notConnected"});
    });
    if(this.state.isConnected !== "connected"){
      setTimeout(()=>{
        this.isConnected();
      }, 3000);
    }
  }

  render() {
    return (
        <View style={styles.containerr}>
          <StatusBar
            backgroundColor="black"
            barStyle="light-content"
          />
          {this.state.isConnected === "connected" &&
            <View style={{justifyContent:"center", alignItems: "center"}}>
              <AdMobBanner
                bannerSize="banner"
                adUnitID="ca-app-pub-7428875399570236/1054378483" // Test ID, Replace with your-admob-unit-id
                testDeviceID="EMULATOR"
                onDidFailToReceiveAdWithError={this.bannerError} />
            </View>
          }
            <Grid>
              <Row>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("drug")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={drugsIcon} style={styles.drugsIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Drug</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("meditation")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={meditationIcon} style={styles.healthIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Meditation</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
              </Row>
              <Row>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("improvement")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={improvementIcon} style={styles.healthIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Improvement</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("health")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={healthIcon} style={styles.healthIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Health</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
              </Row>
              <Row>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("sexuality")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={sexualityIcon} style={styles.healthIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Sexuality</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
                <Col style={styles.col}>
                  <View style={styles.categoryRoww}>
                    <TouchableHighlight onPress={()=>this.onPress("feeling")} underlayColor="transparent">
                      <View style={styles.categoryLinkk}>
                        <View style={styles.imageCoverr}>
                          <Image source={feelingIcon} style={styles.healthIcon}/>
                        </View>
                        <View style={styles.textCoverr}>
                          <DText style={styles.text}>Feeling</DText>
                        </View>
                      </View>
                    </TouchableHighlight>
                  </View>
                </Col>
              </Row>
            </Grid>
          {this.state.isConnected === "connected" &&
            <View style={{justifyContent:"center", alignItems: "center"}}>
              <AdMobBanner
                bannerSize="banner"
                adUnitID="ca-app-pub-7428875399570236/7792683825" // Test ID, Replace with your-admob-unit-id
                testDeviceID="EMULATOR"
                onDidFailToReceiveAdWithError={this.bannerError} />
            </View>
          }
        </View>
    );
  }
}


const styles = StyleSheet.create({
  containerr: {
    alignSelf: 'stretch',
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    flexDirection: 'column',
  },
  backgroundImgg:{
    flex: 1,
    alignSelf: 'stretch',
  },
  categoryLinkk:{
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    width: '100%',
    height: '100%',
    minWidth: 100,
  },
  categoryRoww: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems:'center'
  },
  textCoverr:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  text:{
    fontSize: 22
  },
  col:{
    borderColor: '#d6d7da',
    borderWidth: 1
  },
  healthIcon: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  meditationIcon:{
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  drugsIcon:{
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
  imageCoverr:{
    width:100,
    height: 100,
    marginBottom: -20,
    marginTop:20,
    alignItems: 'center',
    justifyContent: 'center'
  }

});

