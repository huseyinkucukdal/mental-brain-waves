import React from "react";
import styled from "styled-components";
import { Text } from "react-native";

const TextD = styled(Text)`
    fontSize: 14px;
    minHeight: 14px;
    font-family: 'cutive-mono-regular';
`;

export default ({children, ...props})=>{
  return(
    <TextD {...props}>
      {children}
    </TextD>
  )
}
