import React from 'react';
import { Text, AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import PlayListPg from "./pages/PlayListPg";
import PlayPg from "./pages/PlayPg";
import WelcomePg from "./pages/WelcomePg";
import CategoryPg from "./pages/CategoryPg";
import GuidePage from "./pages/GuidePg";
import MakersPg from "./pages/MakersPg";
import { Font } from "expo";
import { MaterialIcons } from '@expo/vector-icons';
import {createStore} from "redux";
import {Provider as ReduxProvider} from "react-redux";
import songLsAll from './assets/SongLst/songLsAll';


writeToDatabase = async(value)=>{
  let selectedValue = value;
  if(typeof value === "object"){
    selectedValue = JSON.stringify(value);
  }
  try{
    console.log("database e  kaydettim1");
    await AsyncStorage.setItem("storageSongList",selectedValue);
    console.log("database e  kaydettim2");
  }catch (er){
    console.log("database kaydetme hatasi");
  }
}

const reducer = (state, action) => {
  if (typeof state === 'undefined') {
    const songs = songLsAll.map((song)=>{
      let selectedSong = {
        name : song.name,
        isLocked: song.isLocked,
        currentNumber: 0,
        totalWatchNumber: song.totalWatchNumber,
        category: song.category,
        uri: song.uri,
        isVideo: song.isVideo,
        duration: song.duration
      }

      return selectedSong;
    })
    return {songListLockState: songs};
  }
  switch(action.type){
    case 'setSongListLockState':
      writeToDatabase(action.newSongList);
      let selectedSongList = action.newSongList;
      if(typeof action.newSongList === "string"){
        selectedSongList = JSON.parse(action.newSongList);
      }
      return {
        songListLockState: selectedSongList,
      }
  }
  return state;
}

const store = createStore(reducer)

const RootStack = createStackNavigator(
  {
    PlayListPage: PlayListPg,
    PlayPage: PlayPg,
    CategoryPage: CategoryPg,
    WelcomePage: WelcomePg,
    GuidePage: GuidePage,
    MakersPage: MakersPg
  },
  {
    initialRouteName: 'WelcomePage'
  }
);

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      fontLoaded: false
    }
  }
  componentDidMount() {
    (async () => {
      await Font.loadAsync({
        ...MaterialIcons.font,
        'cutive-mono-regular': require('./assets/fonts/CutiveMono-Regular.ttf'),
      });

      this.setState({ fontLoaded: true });
    })();
  }
  render() {
    if(!this.state.fontLoaded) return <Text>Loading...</Text>
    return (
      <ReduxProvider store={store}>
        <RootStack/>
      </ReduxProvider>
    );
  }
}
