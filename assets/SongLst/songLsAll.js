class PlaylistItem {
  constructor(name, uri, isVideo, category, duration, isLocked, totalWatchNumber) {
    this.name = name;
    this.uri = uri;
    this.isVideo = isVideo;
    this.category = category;
    this.duration = duration;
    this.isLocked = isLocked;
    this.totalWatchNumber = totalWatchNumber;
  }
}


export default playlist = [
  new PlaylistItem(
    'Positive Endorphin',
    'https://www.pochasoft.com/m45.mp4',
    true,
    'feeling',
    "20:10",
    false,
    0
  ),
  new PlaylistItem(
    'Strong Fear',
    'https://www.pochasoft.com/m28.mp4',
    true,
    'feeling',
    "20:16",
    false,
    0
  ),
  new PlaylistItem(
    'Fly',
    'https://www.pochasoft.com/m57.mp4',
    true,
    'feeling',
    "14:59",
    true,
    1
  ),
  new PlaylistItem(
    'Listen Inner Sound',
    'https://www.pochasoft.com/m48.mp4',
    true,
    'feeling',
    "30:05",
    true,
    2
  ),
  new PlaylistItem(
    'Reduce Anger',
    'https://www.pochasoft.com/m37.mp4',
    true,
    'feeling',
    "20:00",
    true,
    2
  ),
  new PlaylistItem(
    'Relaxation',
    'https://www.pochasoft.com/m53.mp4',
    true,
    'feeling',
    "27:58",
    true,
    3
  ),
  new PlaylistItem(
    'Start To Day',
    'https://www.pochasoft.com/m22.mp4',
    true,
    'feeling',
    "30:51",
    true,
    3
  ),
  new PlaylistItem(
    'Power Nap',
    'https://www.pochasoft.com/m23.mp4',
    true,
    'feeling',
    "30:20",
    true,
    4
  ),
  new PlaylistItem(
    'Quick Energy',
    'https://www.pochasoft.com/m50.mp4',
    true,
    'feeling',
    "10:00",
    true,
    4
  ),
  new PlaylistItem(
    'Stress Relief',
    'https://www.pochasoft.com/m36.mp4',
    true,
    'feeling',
    "20:01",
    true,
    4
  ),
  new PlaylistItem(
    'Love',
    'https://www.pochasoft.com/m32.mp4',
    true,
    'feeling',
    "20:00",
    true,
    5
  ),
  new PlaylistItem(
    'Happiness',
    'https://www.pochasoft.com/m1.mp3',
    false,
    'feeling',
    "71:10",
    true,
    6
  ),
  new PlaylistItem(
    'Bisexual Orgasm',
    'https://www.pochasoft.com/m43.mp4',
    true,
    'sexuality',
    "29:52",
    true,
    4
  ),
  new PlaylistItem(
    'Sex Like on Sky',
    'https://www.pochasoft.com/m60.mp4',
    true,
    'sexuality',
    "30:04",
    true,
    4
  ),
  new PlaylistItem(
    'Nudism',
    'https://www.pochasoft.com/m42.mp4',
    true,
    'sexuality',
    "30:39",
    true,
    4
  ),
  new PlaylistItem(
    'Sexual Lucid Dreaming',
    'https://www.pochasoft.com/m17.mp3',
    false,
    'sexuality',
    "30:10",
    true,
    4
  ),
  new PlaylistItem(
    'Woman Orgasm',
    'https://www.pochasoft.com/m30.mp4',
    true,
    'sexuality',
    "20:00",
    true,
    5
  ),
  new PlaylistItem(
    'Sex Smoothly',
    'https://www.pochasoft.com/m58.mp4',
    true,
    'sexuality',
    "32:30",
    true,
    5
  ),
  new PlaylistItem(
    '10 Minute Male Sexuality',
    'https://www.pochasoft.com/m21.mp4',
    true,
    'sexuality',
    "10:00",
    true,
    5
  ),
  new PlaylistItem(
    'Viagra Effect',
    'https://www.pochasoft.com/m24.mp4',
    true,
    'sexuality',
    "30:00",
    true,
    9
  ),
  new PlaylistItem(
    'Thyroid Healing',
    'https://www.pochasoft.com/m55.mp4',
    true,
    'health',
    "30:18",
    false,
    0
  ),
  new PlaylistItem(
    'Tinnitus Cure',
    'https://www.pochasoft.com/m31.mp4',
    true,
    'health',
    "19:57",
    false,
    0
  ),
  new PlaylistItem(
    'Sinus Healing',
    'https://www.pochasoft.com/m26.mp4',
    true,
    'health',
    "15:00",
    true,
    1
  ),
  new PlaylistItem(
    'Stop Smoking',
    'https://www.pochasoft.com/m13.mp3',
    false,
    'health',
    "20:01",
    true,
    2
  ),
  new PlaylistItem(
    'Headache Tensions',
    'https://www.pochasoft.com/m10.mp3',
    false,
    'health',
    "20:10",
    true,
    3
  ),
  new PlaylistItem(
    'Pain Killer Fast',
    'https://www.pochasoft.com/m15.mp3',
    false,
    'health',
    "30:10",
    true,
    4
  ),
  new PlaylistItem(
    'Sleep Fast',
    'https://www.pochasoft.com/m44.mp4',
    true,
    'health',
    "30:17",
    true,
    4
  ),
  new PlaylistItem(
    'Deep Sleep',
    'https://www.pochasoft.com/m5.mp3',
    false,
    'health',
    "66:10",
    true,
    5
  ),
  new PlaylistItem(
    'Lose Weight',
    'https://www.pochasoft.com/m19.mp3',
    false,
    'health',
    "30:01",
    true,
    6
  ),
  new PlaylistItem(
    'Deep Space',
    'https://www.pochasoft.com/m46.mp4',
    true,
    'improvement',
    "30:00",
    false,
    5
  ),
  new PlaylistItem(
    'Anti Anxiety',
    'https://www.pochasoft.com/m16.mp3',
    false,
    'improvement',
    "10:00",
    true,
    1
  ),
  new PlaylistItem(
    'Creativity',
    'https://www.pochasoft.com/m59.mp4',
    true,
    'improvement',
    "30:00",
    true,
    2
  ),
  new PlaylistItem(
    'Focus',
    'https://www.pochasoft.com/m56.mp4',
    true,
    'improvement',
    "30:15",
    true,
    2
  ),
  new PlaylistItem(
    'Memory Improvement',
    'https://www.pochasoft.com/m51.mp4',
    true,
    'improvement',
    "29:16",
    true,
    2
  ),
  new PlaylistItem(
    'Astral Projection',
    'https://www.pochasoft.com/m2.mp4',
    true,
    'improvement',
    "60:00",
    true,
    3
  ),
  new PlaylistItem(
    'Mental Refresher',
    'https://www.pochasoft.com/m54.mp4',
    true,
    'improvement',
    "15:30",
    true,
    3
  ),
  new PlaylistItem(
    'Super Learning',
    'https://www.pochasoft.com/m39.mp4',
    true,
    'improvement',
    "30:10",
    true,
    3
  ),
  new PlaylistItem(
    'Solve Problems',
    'https://www.pochasoft.com/m34.mp4',
    true,
    'improvement',
    "20:00",
    true,
    4
  ),
  new PlaylistItem(
    'Train Your Brain',
    'https://www.pochasoft.com/m35.mp4',
    true,
    'improvement',
    "10:00",
    true,
    4
  ),
  new PlaylistItem(
    'Anti Depressant Effect',
    'https://www.pochasoft.com/m27.mp4',
    true,
    'improvement',
    "20:00",
    true,
    5
  ),
  new PlaylistItem(
    'Hypnosis',
    'https://www.pochasoft.com/m11.mp4',
    true,
    'improvement',
    "10:00",
    true,
    6
  ),
  new PlaylistItem(
    'Positive Energy',
    'https://www.pochasoft.com/m41.mp4',
    true,
    'meditation',
    "30:30",
    false,
    0
  ),
  new PlaylistItem(
    '432Hz Om Mantra',
    'https://www.pochasoft.com/m25.mp4',
    true,
    'meditation',
    "29:55",
    false,
    0
  ),
  new PlaylistItem(
    '397Hz Ocean',
    'https://www.pochasoft.com/m47.mp4',
    true,
    'meditation',
    "20:00",
    true,
    1
  ),
  new PlaylistItem(
    'Chakra Balancing',
    'https://www.pochasoft.com/m14.mp3',
    false,
    'meditation',
    "34:59",
    true,
    2
  ),
  new PlaylistItem(
    "Third Eye",
    'https://www.pochasoft.com/m4.mp3',
    false,
    'meditation',
    "60:10",
    true,
    3
  ),
  new PlaylistItem(
    'Quantum Awakening',
    'https://www.pochasoft.com/m52.mp4',
    true,
    'meditation',
    "15:00",
    true,
    3
  ),
  new PlaylistItem(
    'Kundalini Meditation',
    'https://www.pochasoft.com/m38.mp4',
    true,
    'meditation',
    "20:01",
    true,
    4
  ),
  new PlaylistItem(
    'Self-Healing Meditation',
    'https://www.pochasoft.com/m18.mp3',
    false,
    'meditation',
    "30:05",
    true,
    4
  ),
  new PlaylistItem(
    'Zen Meditation',
    'https://www.pochasoft.com/m6.mp3',
    false,
    'meditation',
    "56:37",
    true,
    6
  ),
  new PlaylistItem(
    'Feeling High',
    'https://www.pochasoft.com/m8.mp3',
    false,
    'drug',
    "60:40",
    true,
    4
  ),
  new PlaylistItem(
    'Peyote Effect',
    'https://www.pochasoft.com/m3.mp4',
    false,
    'drug',
    "60:00",
    true,
    4
  ),
  new PlaylistItem(
    'Weed Like',
    'https://www.pochasoft.com/m20.mp4',
    true,
    'drug',
    "20:01",
    true,
    4
  ),
  new PlaylistItem(
    'Bonzai Effect',
    'https://www.pochasoft.com/m33.mp4',
    true,
    'drug',
    "15:06",
    true,
    5
  ),
  new PlaylistItem(
    'Heroin Effect',
    'https://www.pochasoft.com/m29.mp4',
    true,
    'drug',
    "19:59",
    true,
    5
  ),

  new PlaylistItem(
    'LSD Effect',
    'https://www.pochasoft.com/m9.mp3',
    false,
    'drug',
    "30:00",
    true,
    5
  ),
  new PlaylistItem(
    'Marijuana Effect',
    'https://www.pochasoft.com/m12.mp3',
    false,
    'drug',
    "20:10",
    true,
    5
  ),
  new PlaylistItem(
    'Meth Effect',
    'https://www.pochasoft.com/m49.mp4',
    true,
    'drug',
    "30:30",
    true,
    5
  ),
  new PlaylistItem(
    'Orgasmic Ecstasy',
    'https://www.pochasoft.com/m7.mp3',
    false,
    'drug',
    "28:40",
    true,
    5
  ),
  new PlaylistItem(
    'Cocaine Effect',
    'https://www.pochasoft.com/m40.mp4',
    true,
    'drug',
    "29:58",
    true,
    9
  )
];
